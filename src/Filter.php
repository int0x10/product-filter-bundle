<?php

namespace Int0x10\ProductFilters;

use Int0x10\ProductFilters\Enums\FilterType;

class Filter implements FilterDefinitionInterface
{
    private string $filterCaption;
    private string $modelProperty;
    private FilterType $filterType;
    private array $variants;

    /**
     * @param string|null $filterCaption Название фильтра (не используется в логике фильтрации, необходимо только для группировки на frontend)
     * @param string|null $modelProperty Свойство модели, по которому будет осуществляться фильтрация (передаётся в get-параметре)
     * @param FilterType|null $filterType Тип фильтра [radio, checkbox, range...] (не используется в логике фильтрации, необходимо только для вывода соответствующего элемента на frontend)
     * @param array|null $variants Массив предоставляемых вариантов фильтра
     */
    public function __construct(?string $filterCaption = null, ?string $modelProperty = null, ?FilterType $filterType = null, ?array $variants = null)
    {
        $this->filterCaption = $filterCaption ?? '';
        $this->modelProperty = $modelProperty ?? '';
        $this->filterType = $filterType ?? FilterType::from(FilterType::DEFAULT);
        $this->variants = $variants ?? [];
    }

    public function setFilterType(FilterType $filterType): FilterDefinitionInterface
    {
        $this->filterType = $filterType;

        return $this;
    }

    public function getFilterType(): FilterType
    {
        return $this->filterType;
    }

    public function setFilterCaption(string $filterCaption): FilterDefinitionInterface
    {
        $this->filterCaption = $filterCaption;

        return $this;
    }

    public function getFilterCaption(): string
    {
        return $this->filterCaption;
    }

    public function setModelProperty(string $modelProperty): FilterDefinitionInterface
    {
        $this->modelProperty = $modelProperty;

        return $this;
    }

    public function getModelProperty(): string
    {
        return $this->modelProperty;
    }

    public function setVariants(array $variants): FilterDefinitionInterface
    {
        $this->variants = $variants;

        return $this;
    }

    public function getVariants(): array
    {
        return $this->variants;
    }
}