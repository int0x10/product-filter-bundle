<?php

namespace Int0x10\ProductFilterBundle;

interface ProductFilterInterface
{
    public function setEntity(string $entityClassName): ProductFilterInterface;
    public function setStrategy(string $strategyId): ProductFilterInterface;
    public function getQuery(): ProductFilterInterface;
    public function getFiltersData(): array;
    public function getFilteredResult(): array;
}