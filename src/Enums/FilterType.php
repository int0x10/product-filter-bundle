<?php

namespace Int0x10\ProductFilters\Enums;

use MyCLabs\Enum\Enum;

class FilterType extends Enum
{
    public const DEFAULT = self::RADIO;

    public const RADIO = 'radio';
    public const CHECKBOX = 'checkbox';
    public const RANGE = 'range';
}