<?php

namespace Int0x10\ProductFilters\Models;

class ProductModel
{
    private string $vendor;

    /**
     * @param string $vendor
     * @return ProductModel
     */
    public function setVendor(string $vendor): ProductModel
    {
        $this->vendor = $vendor;

        return $this;
    }

    /**
     * @return string
     */
    public function getVendor(): string
    {
        return $this->vendor;
    }
}