<?php

namespace Int0x10\ProductFilterBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class Filter
{
    /**
     * @Required
     *
     * @var string
     */
    public string $name;

    public string $type;

    public string $listEntity;

    public string $listProperty;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getListEntity(): string
    {
        return $this->listEntity;
    }

    /**
     * @return string
     */
    public function getListProperty(): string
    {
        return $this->listProperty;
    }
}