<?php

namespace Int0x10\ProductFilterBundle;

use Int0x10\ProductFilterBundle\DependencyInjection\ProductFilterBundleExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class ProductFilterBundle extends Bundle
{
    public function getContainerExtension(): ProductFilterBundleExtension
    {
        if (null === $this->extension) {
            $this->extension = new ProductFilterBundleExtension();
        }

        return $this->extension;
    }
}