<?php

namespace Int0x10\ProductFilterBundle;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Ds\Map;
use Ds\Sequence;
use Ds\Vector;
use Int0x10\ProductFilterBundle\Annotation\Filter;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use Symfony\Component\HttpFoundation\RequestStack;

class ProductFilterBuilder implements ProductFilterInterface
{
    private Reader $annotationReader;
    private string $className;
    private string $strategyId;
    private Map $filterMap;
    private Map $relationMap;
    private Sequence $allowedFilters;
    private array $queryParams;
    private EntityManagerInterface $entityManager;

    public function __construct(Reader $annotationReader, RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->annotationReader = $annotationReader;
        $this->filterMap = new Map();
        $this->relationMap = new Map();
        $this->allowedFilters = new Vector();
        $this->queryParams = $requestStack->getMainRequest()->query->all();
        $this->entityManager = $entityManager;
    }

    public function setEntity(string $entityClassName): ProductFilterInterface
    {
        $this->className = $entityClassName;

        return $this;
    }

    public function setStrategy(string $strategyId): ProductFilterInterface
    {
        $this->strategyId = $strategyId;

        return $this;
    }

    /**
     * @throws ReflectionException
     */
    public function getQuery(): ProductFilterInterface
    {
        $this->discoverAnnotations();

        return $this;
    }

    /**
     * @throws ReflectionException
     */
    private function discoverAnnotations()
    {
        $properties = (new ReflectionClass($this->className))->getProperties();

        array_walk($properties, function (ReflectionProperty $property): void {
            /** @var Filter | null $filter */
            $filter = $this->annotationReader->getPropertyAnnotation($property, Filter::class);

            if ($filter !== null) {

                $this->allowedFilters->push($filter->getName());

                switch ($filter->getType()) {
                    case 'range':
                        $this->filterMap->put($filter->getName(), [
                            'type' => $filter->getType(),
                            'title' => '',
                            'name' => $filter->getName(),
                        ]);
                        break;
                    case 'checkbox':
                        $this->relationMap->put($filter->getName(), $filter->getListProperty());
                        $this->filterMap->put($filter->getName(), [
                            'type' => $filter->getType(),
                            'title' => '',
                            'name' => $filter->getName(),
                            'variants' => array_column(
                                $this->entityManager
                                    ->getRepository($filter->getListEntity())
                                    ->createQueryBuilder('b')
                                    ->select('b.' . $filter->getListProperty())
                                    ->andWhere('b.' . $filter->getListProperty() . ' != :empty')->setParameter('empty', '')
                                    ->getQuery()
                                    ->getResult(),
                                $filter->getListProperty()
                            )
                        ]);
                        break;
                }
            }
        });
    }


    public function setItemEntity(string $entityClassName): ProductFilterInterface
    {
        $this->className = $entityClassName;

        return $this;
    }

    /**
     * Возвращает массив свойств модели, по которым производится фильтрация
     *
     *
     * @return array
     */
    public function getAllowedFilterProperties(): array
    {
        return $this->allowedFilters->toArray();
    }

    /**
     * Возвращает массив фильтров, их типов и значений
     *
     *
     * @return array
     */
    public function getFiltersData(): array
    {
        return $this->filterMap->toArray();
    }

    /**
     * Возвращает отфильтрованный результат по query-params
     *
     *
     * @return array
     * @throws \Exception
     */
    public function getFilteredResult(): array
    {
        $builder = $this->entityManager->getRepository($this->className)
            ->createQueryBuilder('b');

        foreach ($this->queryParams as $paramKey => $queryParams) {
            if ($this->allowedFilters->contains($paramKey)) {
                // Тип фильтра range | checkbox
                $filterType = $this->filterMap->get($paramKey)['type'];

                switch ($filterType) {
                    case 'range':
                        // Для типа range всегда должно быть два параметра
                        // От и до
                        if (!is_array($queryParams) || count($queryParams) <=> 2) {
                            throw new \Exception('Для типа range (' . $paramKey . ') должны быть заданы два значения');
                        }

                        $builder
                            ->andWhere('b.' . $paramKey . ' >= ' . (int)$queryParams[0])
                            ->andWhere('b.' . $paramKey . ' <= ' . (int)$queryParams[1]);
                        break;

                    case 'checkbox':
                        $relationName = $this->relationMap->get($paramKey);
                        $builder
                            ->join('b.' . $paramKey, $paramKey)
                            ->andWhere($paramKey . '.' . $relationName . ' IN (:values)')->setParameter('values', $queryParams);
                        break;
                }
            }
        }


//        $result = $this->entityManager->getRepository($this->className)
//            ->createQueryBuilder('i')
//            ->join('i.section', 's')
//            ->where('s.slug = :slug')
//            ->setParameter('slug', $this->strategyId);
//
//
//        if (array_key_exists('manufacturer', $this->queryParams)) {
//
//            $result
//                ->join('i.manufacturer', 'm')
//                ->andWhere('m.name = :manufacturer')
//                ->setParameter('manufacturer', $this->queryParams['manufacturer']);
//        }

        return $builder->getQuery()->getResult();
    }
}