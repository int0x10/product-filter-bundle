<?php declare(strict_types=1);

use Ds\Sequence;
use Ds\Vector;
use Faker\Factory;
use Int0x10\ProductFilters\Enums\FilterType;
use Int0x10\ProductFilters\Filter;
use Int0x10\ProductFilters\Models\ProductModel;
use Int0x10\ProductFilters\ProductFilterBuilder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

require __DIR__ . '/../vendor/autoload.php';

final class ProductFilterBuilderTest extends TestCase
{
    private ProductFilterBuilder $productFilterBuilder;
    private \Faker\Generator $faker;

    private Sequence $models;

    private string $filterCaption1;
    private string $filterCaption2;
    private string $filterProperty1;
    private string $filterProperty2;

    private function createSecondaryFilter(): Filter
    {
        return (new Filter())
            ->setFilterCaption($this->filterCaption2)
            ->setModelProperty($this->filterProperty2)
            ->setVariants(
                $this->models->map(function (ProductModel $model): string {
                    return $model->getVendor();
                })->slice(0, 3)->toArray()
            )
            ->setFilterType(new FilterType(FilterType::RADIO));
    }

    protected function setUp(): void
    {
        $this->faker = Factory::create();

        $this->filterCaption1 = $this->faker->text(8);
        $this->filterCaption2 = $this->faker->text(8);
        $this->filterProperty1 = 'color';
        $this->filterProperty2 = 'vendor';

        $this->models = new Vector();
        for ($i = 1; $i <= 10; $i++) {
            $this->models->push((new ProductModel())
                ->setVendor($this->faker->company)
            );
        }

        $secondaryFilter = $this->createSecondaryFilter();
        $request = Request::create(
            '/vendors',
            'GET',
            [
                'a' => $secondaryFilter->getVariants(),
                $secondaryFilter->getModelProperty() => $secondaryFilter->getVariants(),
            ]
        );

        $this->productFilterBuilder = (new ProductFilterBuilder())
            ->setRequest($request)
            ->addFilter(
                (new Filter($this->filterCaption1, $this->filterProperty1, new FilterType(FilterType::CHECKBOX), $this->models->map(function (ProductModel $model) {
                    return $model->getVendor();
                })->slice(0, 3)->toArray()))
            )
            ->addFilter($secondaryFilter);
    }

    public function testGetAllowedFilterProperties(): void
    {
        $array = $this->productFilterBuilder->getAllowedFilterProperties();

        $this->assertEqualsCanonicalizing([$this->filterProperty1, $this->filterProperty2], $array);
    }

//    public function testGetFiltersData(): void
//    {
//        $array = $this->productFilterBuilder->getFiltersData();
//
//        dump($array);
//    }

    public function testGetFilteredResult(): void
    {
        $array = $this->productFilterBuilder->getFilteredResult();

        dump($array);
    }
}